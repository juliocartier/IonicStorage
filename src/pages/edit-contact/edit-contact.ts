import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ContactoProvider, Contact } from '../../providers/contacto/contacto';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the EditContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-contact',
  templateUrl: 'edit-contact.html',
})
export class EditContactPage {
  
  model: Contact;
  key: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private contactoProvider: ContactoProvider, private toast: ToastController, private storage: Storage) {
    
    if (this.navParams.data.contact && this.navParams.data.key) {
      this.model = this.navParams.data.contact;
      this.key =  this.navParams.data.key;
      //console.log(this.key);
    } else {
      this.model = new Contact();
      //this.funcaoTeste();
      //console.log(this.model);
    }

  }

  save() {
    this.saveContact()
      .then(() => {
        this.toast.create({ message: 'Contato salvo.', duration: 3000, position: 'botton' }).present();
        this.navCtrl.pop();
      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao salvar o contato.', duration: 3000, position: 'botton' }).present();
      });
  }
 
  private saveContact() {
    if (this.key) {
      //console.log(this.model.name);
      return this.contactoProvider.update(this.key, this.model);
    } else {
      return this.contactoProvider.insert(this.model);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditContactPage');
  }

  funcaoTeste(){
    this.storage.set('name', 'Julio');

  // Or to get a key/value pair
    this.storage.get('name').then((val) => {
      console.log('Your name is', val);
    });
  }

}
