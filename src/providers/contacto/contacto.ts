import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';

/*
  Generated class for the ContactoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContactoProvider {

  constructor(private storage: Storage, private datepipe: DatePipe) {
    console.log('Hello ContactoProvider Provider');
  }

  public insert(contact: Contact) {
    //let key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
    let key = contact.name;
    //let key = name;
    return this.save(key, contact);
  }
 
  public update(key: string, contact: Contact) {
    return this.save(key, contact);
  }
 
  private save(key: string, contact: Contact) {
    return this.storage.set(key, contact);
  }
 
  public remove(key: string) {
    return this.storage.remove(key);
  }
 
  public getAll() {
 
    let contacts: ContactList[] = [];
 
    return this.storage.forEach((value: Contact, key: string, iterationNumber: Number) => {
      let contact = new ContactList();
      contact.key = key;
      contact.contact = value;
      contacts.push(contact);
    })
      .then(() => {
        return Promise.resolve(contacts);
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  }

}

export class Contact {
  name: string;
  phone: number;
 // id: string;
  active: boolean;
}
 
export class ContactList {
  key: string;
  contact: Contact;
}